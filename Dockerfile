# init a base image (Alpine is small Linux distro)
FROM python:3.6.1-alpine
# define the present working directory
WORKDIR /Assignment-4
# copy the contents into the working dir
ADD . /Assignment-4
# run pip to install the dependencies of the flask app
RUN pip install -r requirements.txt
EXPOSE 5000
# define the command to start the container
CMD ["python","application.py"]